/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.objects;

import net.ihe.gazelle.cache.CacheObjectUpdater;

/**
 * <b>Class Description : </b>KeyValueCacheManager<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 28/01/16
 *
 */
public class ObjectCacheManager implements ObjectCache {

    private ObjectCache cache;

    public ObjectCacheManager(ObjectCache cache) {
        this.cache = cache;
    }

    public ObjectCache getCache() {
        return cache;
    }

    @Override
    public Object getValue(String key, CacheObjectUpdater cacheUpdater, Object... parameter) {
        return getCache().getValue(key, cacheUpdater, parameter);
    }

    @Override
    public void removeValue(String key) {
        getCache().removeValue(key);
    }

    @Override
    public Object getValue(String key) {
        return getCache().getValue(key);
    }

    @Override
    public void setValue(String key, Object value) {
        getCache().setValue(key, value);
    }

    @Override
    public int getExpireTimeOut() {
        return getCache().getExpireTimeOut();
    }
}
