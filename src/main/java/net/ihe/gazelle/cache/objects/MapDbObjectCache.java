/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.objects;

import net.ihe.gazelle.cache.CacheObjectUpdater;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import java.util.concurrent.TimeUnit;

/**
 * <b>Class Description : </b>MapDbCache<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class MapDbObjectCache implements ObjectCache {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MapDbObjectCache.class);
    private static final int DEFAULT_EXPIRE_TIME = 10;
    private static final int MAX_CACHE_SIZE = 10;
    private static MapDbObjectCache instance = new MapDbObjectCache(DEFAULT_EXPIRE_TIME);
    private int expireTimeOut;
    private HTreeMap<Object, Object> cache;
    private DB db = DBMaker.memoryDB().closeOnJvmShutdown().make();

    public MapDbObjectCache(int expireTimeOut) {
        this.expireTimeOut = expireTimeOut;
        cache = db.hashMapCreate("default")
                .expireAfterWrite(getExpireTimeOut(), TimeUnit.SECONDS)
                .expireAfterAccess(getExpireTimeOut(), TimeUnit.SECONDS)
                .expireMaxSize(MAX_CACHE_SIZE)
                .makeOrGet();
    }

    public static MapDbObjectCache getInstance() {
        return instance;
    }

    @Override
    public Object getValue(String key, CacheObjectUpdater cacheUpdater, Object... parameter) {
        Object value = getValue(key);
        if (value == null) {
            value = cacheUpdater.getValue(key, parameter);
            setValue(key, value);
            LOG.warn("MapDbObjectCache miss:" + key);
        } else {
            LOG.warn("MapDbObjectCache hit:" + key);
        }
        return value;
    }

    @Override
    public void removeValue(String key) {
        cache.remove(key);
    }

    @Override
    public Object getValue(String key) {
        return cache.get(key);
    }

    @Override
    public void setValue(String key, Object value) {
        cache.put(key, value);
    }

    @Override
    public final int getExpireTimeOut() {
        return expireTimeOut;
    }

    public HTreeMap<Object, Object> getCache() {
        return cache;
    }
}
