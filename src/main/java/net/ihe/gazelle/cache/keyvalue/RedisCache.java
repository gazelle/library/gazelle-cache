/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import net.ihe.gazelle.cache.CacheUpdater;
import redis.clients.jedis.JedisCommands;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.MultiKeyCommands;

import java.util.Set;

/**
 * <b>Class Description : </b>RedisCache<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class RedisCache implements KeyValueCache {
    private static final String DEFAULT = "default:";
    private JedisPool pool;
    private int expireTimeOut = -1;
    private String cacheKeyPrefix = DEFAULT;
    private boolean enable = true;

    private JedisCommands nullRedisCache = new NullRedis();

    public RedisCache() {
        pool = new JedisPool(new JedisPoolConfig(), "localhost");
    }

    public RedisCache(int expireTimeOut) {
        pool = new JedisPool(new JedisPoolConfig(), "localhost");
        this.expireTimeOut = expireTimeOut;
    }

    public RedisCache(int expireTimeOut, String host) {
        pool = new JedisPool(new JedisPoolConfig(), host);
        this.expireTimeOut = expireTimeOut;
    }

    public RedisCache(int expireTimeOut, String host, int port) {
        pool = new JedisPool(new JedisPoolConfig(), host, port);
        this.expireTimeOut = expireTimeOut;
    }

    public void setCacheKeyPrefix(String prefix) {
        cacheKeyPrefix = prefix;
    }

    private String getRedisKey(String key) {
        return cacheKeyPrefix + key;
    }

    private JedisCommands getCache() {
        if (isEnable()) {
            return pool.getResource();
        } else {
            return nullRedisCache;
        }
    }

    @Override
    public String getValue(String key, CacheUpdater cacheUpdater, Object... parameter) {
        String value = getValue(key);
        if (value == null) {
            value = cacheUpdater.getValue(key, parameter);
            setValue(key, value);
        }
        return value;
    }

    @Override
    public void removeValue(String key) {
        getCache().del(getRedisKey(key));
    }

    @Override
    public String getValue(String key) {
        return getCache().get(getRedisKey(key));
    }

    @Override
    public void setValue(String key, String value) {
        getCache().set(getRedisKey(key), value);
        if (getExpireTimeOut() != -1) {
            getCache().expire(getRedisKey(key), getExpireTimeOut());
        }
    }

    public void setValue(String key, String value, int expireInSeconds) {
        getCache().set(getRedisKey(key), value);
        getCache().expire(getRedisKey(key), expireInSeconds);
    }

    @Override
    public int getExpireTimeOut() {
        return expireTimeOut;
    }

    @Override
    public void clearCache() {
        Set<String> redisKeys = ((MultiKeyCommands) getCache()).keys(cacheKeyPrefix);
        for (String key : redisKeys) {
            getCache().del(key);
        }
    }

    @Override
    public boolean isEnable() {
        return enable;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void enable() {
        enable = true;
    }

    @Override
    public void disable() {
        enable = false;
    }
}
