/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import net.ihe.gazelle.cache.CacheUpdater;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * <b>Class Description : </b>MapDbCache<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class MapDbCache implements KeyValueCache {
    private static final int DEFAULT_EXPIRE_TIME = 10;
    private static Logger log = LoggerFactory.getLogger(MapDbCache.class);
    private int expireTimeOut;
    private ConcurrentMap<String, Object> cache;
    private DB db = DBMaker.memoryDB().closeOnJvmShutdown().make();
    private ConcurrentMap<String, Object> nullMapDbCache = new NullMapDbCache<String, Object>();

    private boolean enable = true;

    public MapDbCache() {
        this.expireTimeOut = DEFAULT_EXPIRE_TIME;
        getCache();
    }

    public MapDbCache(int expireTimeOut) {
        this.expireTimeOut = expireTimeOut;
        getCache();
    }

    @Override
    public String getValue(String key, CacheUpdater cacheUpdater, Object... parameter) {
        String value = getValue(key);
        if (value == null) {
            value = cacheUpdater.getValue(key, parameter);
            if (value != null) {
                setValue(key, value);
            }
            log.info("MapDbObjectCache miss:" + key);
        }
        return value;
    }

    @Override
    public void removeValue(String key) {
        getCache().remove(key);
    }

    @Override
    public String getValue(String key) {
        return (String) getCache().get(key);
    }

    @Override
    public void setValue(String key, String value) {
        getCache().put(key, value);
    }

    @Override
    public final int getExpireTimeOut() {
        return expireTimeOut;
    }

    @Override
    public void clearCache() {
        getCache().clear();
    }

    @Override
    public final boolean isEnable() {
        return enable;
    }

    @Override
    public void enable() {
        enable = true;
    }

    @Override
    public void disable() {
        enable = false;
    }

    protected final ConcurrentMap<String, Object> getCache() {
        if (isEnable()) {
            if (cache == null) {
                cache = db.hashMapCreate("default")
                        .keySerializer(Serializer.STRING)
                        .expireAfterWrite(getExpireTimeOut(), TimeUnit.MINUTES)
                        .expireAfterAccess(getExpireTimeOut(), TimeUnit.MINUTES)
                        .expireStoreSize(1)
                        .makeOrGet();
            }
        } else {
            return nullMapDbCache;
        }
        return cache;
    }

    public int size() {
        return getCache().size();
    }

    public void commit() {
        db.commit();
    }
}
