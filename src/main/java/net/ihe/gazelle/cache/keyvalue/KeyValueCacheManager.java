/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import net.ihe.gazelle.cache.CacheUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description : </b>KeyValueCacheManager<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 28/01/16
 *
 */
public class KeyValueCacheManager implements KeyValueCache {
    private static Logger log = LoggerFactory.getLogger(KeyValueCacheManager.class);
    private KeyValueCache cache;

    public KeyValueCacheManager(KeyValueCache cache) {
        this.cache = cache;
    }

    public KeyValueCache getCache() {
        return cache;
    }

    @Override
    public String getValue(String key, CacheUpdater cacheUpdater, Object... parameter) {
        return getCache().getValue(key, cacheUpdater, parameter);
    }

    @Override
    public void removeValue(String key) {
        getCache().removeValue(key);
    }

    @Override
    public String getValue(String key) {
        return getCache().getValue(key);
    }

    @Override
    public void setValue(String key, String value) {
        getCache().setValue(key, value);
    }

    @Override
    public int getExpireTimeOut() {
        return getCache().getExpireTimeOut();
    }

    @Override
    public void clearCache() {
        log.warn("Clear Cache");
        getCache().clearCache();
    }

    @Override
    public void enable() {
        getCache().enable();
    }

    @Override
    public void disable() {
        getCache().disable();
    }

    @Override
    public boolean isEnable() {
        return getCache().isEnable();
    }

    @Override
    public int size() {
        return getCache().size();
    }
}
