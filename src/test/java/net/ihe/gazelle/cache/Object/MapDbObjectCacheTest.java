/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.Object;

import net.ihe.gazelle.cache.CacheObjectUpdater;
import net.ihe.gazelle.cache.objects.MapDbObjectCache;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * <b>Class Description : </b>MapDbObjectCacheTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class MapDbObjectCacheTest {
    MapDbObjectCache cache;

    @Before
    public void initialize() {
        cache = MapDbObjectCache.getInstance();
        cache.getCache().clear();
    }

    @Test
    public void cacheExists() throws Exception {
        assertNotNull(cache);
    }

    @Test
    public void onlyOneCacheIsUsed() throws Exception {
        MapDbObjectCache otherInstance = MapDbObjectCache.getInstance();
        MapDbObjectCache thirdInstance = MapDbObjectCache.getInstance();
        assertEquals(cache.getCache(), otherInstance.getCache());
        cache.setValue("key", new ObjectToSave());
        assertEquals("objectToSaveValue", ((ObjectToSave) otherInstance.getValue("key")).getData());
        assertEquals("objectToSaveValue", ((ObjectToSave) thirdInstance.getValue("key")).getData());
    }

    @Test
    public void testGetValue() throws Exception {
        cache.getValue("toUpdate", new CacheObjectUpdater() {
            @Override
            public Object getValue(String key, Object... parameter) {
                return new ObjectToSave();
            }
        }, null);

        assertEquals("objectToSaveValue", ((ObjectToSave) cache.getValue("toUpdate")).getData());
    }

    @Test
    public void testSetValue() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));
    }

    @Test
    public void testSetObject() throws Exception {
        cache.setValue("ocManager", new ObjectToSave());
        assertEquals("objectToSaveValue", ((ObjectToSave) cache.getValue("ocManager")).getData());
    }

    private class ObjectToSave implements Serializable {

        private static final long serialVersionUID = -7314964872643110117L;
        private String data = "objectToSaveValue";
        private String data2 = "objectToSaveValue2";

        public String getData() {
            return data;
        }

        public String getData2() {
            return data2;
        }
    }
}