/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.Object;

import net.ihe.gazelle.cache.objects.MapDbObjectCache;
import net.ihe.gazelle.cache.objects.ObjectCacheManager;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * <b>Class Description : </b>ObjectCacheManagerTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 28/01/16
 *
 */
public class ObjectCacheManagerTest {

    @Test
    public void managerWorksWithRedisCache() {
        ObjectCacheManager ocManager = new ObjectCacheManager(MapDbObjectCache.getInstance());
        assertNotNull(ocManager.getCache());
    }

    @Test
    public void managerWorksWithMapDbCache() {
        ObjectCacheManager ocManager = new ObjectCacheManager(MapDbObjectCache.getInstance());
        assertNotNull(ocManager.getCache());
    }

    @Test
    public void managerDelegatesToCache() {
        ObjectCacheManager ocManager = new ObjectCacheManager(MapDbObjectCache.getInstance());
        assertNotNull(ocManager.getCache());
        ocManager.setValue("ocManager", new ObjectToSave());
        assertEquals("objectToSaveValue", ((ObjectToSave) ocManager.getValue("ocManager")).getData());
    }

    private class ObjectToSave implements Serializable {

        private static final long serialVersionUID = -7314964872643110117L;
        String data = "objectToSaveValue";

        public String getData() {
            return data;
        }
    }
}