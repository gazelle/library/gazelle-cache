/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import net.ihe.gazelle.cache.CacheUpdater;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * <b>Class Description : </b>RedisCacheTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class RedisCacheTest {
    RedisCache cache = new RedisCache();

    @Test
    public void cacheExists() throws Exception {
        assertNotNull(cache);
    }

    @Test
    public void onlyOneCacheExists() throws Exception {
        RedisCache otherInstance = new RedisCache();
        cache.setValue("key", "onlyOneCacheIsUsed");
        assertEquals("onlyOneCacheIsUsed", otherInstance.getValue("key"));
    }

    @Test
    public void testGetValue() throws Exception {
        cache.getValue("toUpdate", new CacheUpdater() {
            @Override
            public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superTest");
        assertEquals("superTest", cache.getValue("toUpdate"));
    }

    @Test
    public void testSetValue() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));
    }

    @Test
    public void testSetExpireValue() throws Exception {
        cache.setValue("key", "value", 1);
        assertEquals("value", cache.getValue("key"));
        TimeUnit.SECONDS.sleep(2);
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void testSetExpireDefaultValue() throws Exception {
        RedisCache cache = new RedisCache(1);
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));
        TimeUnit.SECONDS.sleep(2);
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void testRemoveFromCache() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.removeValue("key");
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void disableCache() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void disabledCacheReturnCacheUpdater() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        String result = cache.getValue("key", new CacheUpdater() {
            @Override
            public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superRedis");

        assertEquals("superRedis", result);
    }

    @Test
    public void disabledThenEnabledCache() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        String result = cache.getValue("key", new CacheUpdater() {
            @Override
            public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superRedis");

        assertEquals("superRedis", result);
        cache.enable();
        assertEquals("value", cache.getValue("key"));
    }
}