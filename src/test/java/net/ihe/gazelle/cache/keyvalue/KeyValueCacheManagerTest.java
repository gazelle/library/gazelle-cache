/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * <b>Class Description : </b>KeyValueCacheManagerTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 28/01/16
 *
 */
public class KeyValueCacheManagerTest {

    @Test
    public void managerWorksWithRedisCache() {
        KeyValueCacheManager kvcManager = new KeyValueCacheManager(new RedisCache());
        assertNotNull(kvcManager.getCache());
    }

    @Test
    public void managerWorksWithMapDbCache() {
        KeyValueCacheManager kvcManager = new KeyValueCacheManager(new MapDbCache());
        assertNotNull(kvcManager.getCache());
    }

    @Test
    public void managerDelegatesToCache() {
        KeyValueCacheManager kvcManager = new KeyValueCacheManager(new MapDbCache());
        assertNotNull(kvcManager.getCache());
        kvcManager.setValue("kvcManager", "mapDb");
        assertEquals("mapDb", kvcManager.getValue("kvcManager"));
    }
}