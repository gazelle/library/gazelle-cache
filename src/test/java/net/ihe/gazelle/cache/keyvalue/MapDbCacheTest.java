/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cache.keyvalue;

import net.ihe.gazelle.cache.CacheUpdater;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * <b>Class Description : </b>MapDbCacheTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 27/01/16
 *
 */
public class MapDbCacheTest {
    MapDbCache cache;

    @Before
    public void initialize() {
        cache = new MapDbCache();
        cache.getCache().clear();
    }

    @Test
    public void cacheExists() throws Exception {
        assertNotNull(cache);
    }

    @Test
    public void testGetValue() throws Exception {
        cache.getValue("toUpdate", new CacheUpdater() {
             public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superTest");

        assertEquals("superTest", cache.getValue("toUpdate"));
    }

    @Test
    public void testHandlesNullValueFromUpdater() throws Exception {
        cache.getValue("toUpdate", new CacheUpdater() {
            public String getValue(String key, Object... parameter) {
                return null;
            }
        }, null);

        assertEquals(null, cache.getValue("toUpdate"));
    }

    @Test
    public void testSetValue() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));
    }

    @Test
    public void testRemoveValue() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.removeValue("key");
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void disableCache() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        assertEquals(null, cache.getValue("key"));
    }

    @Test
    public void disabledCacheReturnCacheUpdater() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        String result = cache.getValue("key", new CacheUpdater() {
            @Override
            public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superTest");

        assertEquals("superTest", result);
    }

    @Test
    public void disabledThenEnabledCache() throws Exception {
        cache.setValue("key", "value");
        assertEquals("value", cache.getValue("key"));

        cache.disable();
        String result = cache.getValue("key", new CacheUpdater() {
            @Override
            public String getValue(String key, Object... parameter) {
                return (String) parameter[0];
            }
        }, "superTest");

        assertEquals("superTest", result);
        cache.enable();
        assertEquals("value", cache.getValue("key"));
    }

    @Test
    public void concurrentAccessToCache() throws Exception {
        final int numIter = 100000;
        MapDbCache cache = new MapDbCache();

        Random r = new Random();
        for (int i = 1; i <= numIter; i++) {
            String key2 = "key" + i;
            cache.setValue(key2, i + "val");
        }

        assertEquals(numIter, cache.getCache().size());
        cache.commit();
    }
}